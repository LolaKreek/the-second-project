import React from 'react';

//Creating one skill in the block "About Me" -> "Skills"
//ARGUMENTS:
//class -> class name
//text  -> text in the skill
const Skills = (props) => {
    return(
        <div className={props.class}>
            <p>{props.text}</p>
        </div>
    );
}

export default Skills;