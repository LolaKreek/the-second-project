import {BrowserRouter as Router, Switch} from 'react-router-dom';
import './styles/themes/default/theme.scss';
import React from 'react';
import Homepage from "./pages/homepage";

function App() {

  return (
      <Router>
          <Switch>
                <Homepage />
          </Switch>
      </Router>
  );
}

export default App;
